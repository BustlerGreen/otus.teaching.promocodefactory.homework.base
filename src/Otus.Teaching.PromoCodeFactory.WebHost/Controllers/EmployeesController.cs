﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController  : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _roleRepository;

        public EmployeesController(IRepository<Employee> employeeRepository, IRepository<Role> roleRepository)
        {
            _employeeRepository = employeeRepository;
            _roleRepository = roleRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null) return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }
        [HttpPost]
        public async Task<ActionResult> PostEmployeesAsync(EmployeeRec emp)
        { 
            if (null == emp) return NoContent();
            Guid nguid = Guid.NewGuid();

            var nRoles = await _roleRepository.GetAllAsync();
            var roles = nRoles.Where(r => emp.Roles.Contains(r.Id)).ToList();

            _employeeRepository.Add(new Employee()
            {
                Id = nguid,
                FirstName = emp.FirstName,
                LastName = emp.LastName,
                Email = emp.Email,
                Roles = roles,
                AppliedPromocodesCount = 0
            });
            return Ok();
        }

        [HttpPut]
        public async Task<ActionResult> PutEmployeeAsync(Guid id, EmployeeRec emp)
        {
            if (null == emp) return NoContent();
            var employee = await _employeeRepository.GetByIdAsync(id);
            if (null == employee) return NotFound();

            var nRoles = await _roleRepository.GetAllAsync();
            var roles = nRoles.Where(r => emp.Roles.Contains(r.Id)).ToList();

            _employeeRepository.Update(new Employee()
            {
                Id = id,
                FirstName = emp.FirstName,
                LastName = emp.LastName,
                Email = emp.Email,
                Roles = roles,
                AppliedPromocodesCount = emp.AppliedPromocodesCount
            });

            return Ok();
        }
        [HttpDelete]
        public async Task<ActionResult> DeleteEmployeeAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);
            if (null == employee) return NotFound();
            _employeeRepository.Delete(id);
            return Ok();
        }
    }
}